composer require symfony/web-server-bundle --dev
composer require annotations
composer require symfony/orm-pack
composer require symfony/form
composer require symfony/maker-bundle --dev
composer require symfony/security-bundle
composer require symfony/twig-bundle
composer require symfony/var-dumper --dev
composer require symfony/translation
composer require spipu/html2pdf
composer require league/csv
composer require symfony/swiftmailer-bundle