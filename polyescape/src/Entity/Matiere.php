<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Matiere
 *
 * @ORM\Table(name="MATIERE")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MatiereRepository")
 */
class Matiere
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nommat", type="string", length=100, nullable=true)
     */
    private $nommat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Professeur", mappedBy="idmat")
     */
    private $idprof;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idprof = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function __toString()
{
    return (string) $this->getIdmat();
}

    public function getIdmat(): ?int
    {
        return $this->idmat;
    }

    public function getNommat(): ?string
    {
        return $this->nommat;
    }

    public function setNommat(?string $nommat): self
    {
        $this->nommat = $nommat;

        return $this;
    }

    /**
     * @return Collection|Professeur[]
     */
    public function getIdprof(): Collection
    {
        return $this->idprof;
    }

    public function addIdprof(Professeur $idprof): self
    {
        if (!$this->idprof->contains($idprof)) {
            $this->idprof[] = $idprof;
            $idprof->addIdmat($this);
        }

        return $this;
    }

    public function removeIdprof(Professeur $idprof): self
    {
        if ($this->idprof->contains($idprof)) {
            $this->idprof->removeElement($idprof);
            $idprof->removeIdmat($this);
        }

        return $this;
    }

}
