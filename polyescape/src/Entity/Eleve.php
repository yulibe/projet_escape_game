<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Eleve
 *
 * @ORM\Table(name="ELEVE", indexes={@ORM\Index(name="idpers", columns={"idpers"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\EleveRepository")
 */
class Eleve implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="netu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $netu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="classe", type="string", length=20, nullable=true)
     */
    private $classe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="datec", type="string", length=10, nullable=true)
     */
    private $datec;

    /**
     * @var \Personne
     *
     * @ORM\ManyToOne(targetEntity="Personne")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpers", referencedColumnName="idpers")
     * })
     */
    private $idpers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Groupe", mappedBy="netu")
     * 
     */
    private $idgr;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idgr = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getNetu(): ?int
    {
        return $this->netu;
    }

    public function getClasse(): ?string
    {
        return $this->classe;
    }

    public function setClasse(?string $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getDatec(): ?string
    {
        return $this->datec;
    }

    public function setDatec(?string $datec): self
    {
        $this->datec = $datec;

        return $this;
    }

    public function getIdpers(): ?Personne
    {
        return $this->idpers;
    }

    public function setIdpers(?Personne $idpers): self
    {
        $this->idpers = $idpers;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getIdgr(): Collection
    {
        return $this->idgr;
    }

    public function addIdgr(Groupe $idgr): self
    {
        if (!$this->idgr->contains($idgr)) {
            $this->idgr[] = $idgr;
            $idgr->addNetu($this);
        }

        return $this;
    }

    public function removeIdgr(Groupe $idgr): self
    {
        if ($this->idgr->contains($idgr)) {
            $this->idgr->removeElement($idgr);
            $idgr->removeNetu($this);
        }

        return $this;
    }


    public function getUsername()
    {
        return $this->idpers->getEmail();
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->idpers->getMdp();
    }

    public function getRoles()
    {
        return array('ROLE_STUDENT');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->idpers->email,
            $this->idpers->email,
            $this->idpers->mdp,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->idpers->email,
            $this->idpers->email,
            $this->idpers->mdp,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function AuthValid($credentials)
    {
        return hash("sha512", $credentials["password"]) === $this->idpers->getMdp();
    }


}
