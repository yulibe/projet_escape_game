<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contient
 *
 * @ORM\Table(name="CONTIENT", indexes={@ORM\Index(name="idsalle", columns={"idsalle"}), @ORM\Index(name="idcadena", columns={"idcadena"}), @ORM\Index(name="IDX_1A03B21A1A26B72F", columns={"iddm"})})
 * @ORM\Entity
 */
class Contient
{
    /**
     * @var \Salle
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Salle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsalle", referencedColumnName="idsalle")
     * })
     */
    private $idsalle;

    /**
     * @var \Cadena
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Cadena")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcadena", referencedColumnName="idcadena")
     * })
     */
    private $idcadena;

    /**
     * @var \Dm
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Dm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddm", referencedColumnName="iddm")
     * })
     */
    private $iddm;

    public function getIdsalle(): ?Salle
    {
        return $this->idsalle;
    }

    public function setIdsalle(?Salle $idsalle): self
    {
        $this->idsalle = $idsalle;

        return $this;
    }

    public function getIdcadena(): ?Cadena
    {
        return $this->idcadena;
    }

    public function setIdcadena(?Cadena $idcadena): self
    {
        $this->idcadena = $idcadena;

        return $this;
    }

    public function getIddm(): ?Dm
    {
        return $this->iddm;
    }

    public function setIddm(?Dm $iddm): self
    {
        $this->iddm = $iddm;

        return $this;
    }


}
