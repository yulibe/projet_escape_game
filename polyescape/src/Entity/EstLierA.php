<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstLierA
 *
 * @ORM\Table(name="EST_LIER_A", indexes={@ORM\Index(name="idenigme", columns={"idenigme"}), @ORM\Index(name="idobjet", columns={"idobjet"}), @ORM\Index(name="EST_LIER_A_ibfk_4", columns={"idsalle"}), @ORM\Index(name="IDX_6442F201A26B72F", columns={"iddm"})})
 * @ORM\Entity
 */
class EstLierA
{
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetrouve", type="date", nullable=true)
     */
    private $datetrouve;

    /**
     * @var \Enigme
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Enigme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idenigme", referencedColumnName="idenigme")
     * })
     */
    private $idenigme;

    /**
     * @var \Objet
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Objet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idobjet", referencedColumnName="idobjet")
     * })
     */
    private $idobjet;

    /**
     * @var \Dm
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Dm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddm", referencedColumnName="iddm")
     * })
     */
    private $iddm;

    /**
     * @var \Salle
     *
     * @ORM\ManyToOne(targetEntity="Salle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idsalle", referencedColumnName="idsalle")
     * })
     */
    private $idsalle;

    public function getDatetrouve(): ?\DateTimeInterface
    {
        return $this->datetrouve;
    }

    public function setDatetrouve(?\DateTimeInterface $datetrouve): self
    {
        $this->datetrouve = $datetrouve;

        return $this;
    }

    public function getIdenigme(): ?Enigme
    {
        return $this->idenigme;
    }

    public function setIdenigme(?Enigme $idenigme): self
    {
        $this->idenigme = $idenigme;

        return $this;
    }

    public function getIdobjet(): ?Objet
    {
        return $this->idobjet;
    }

    public function setIdobjet(?Objet $idobjet): self
    {
        $this->idobjet = $idobjet;

        return $this;
    }

    public function getIddm(): ?Dm
    {
        return $this->iddm;
    }

    public function setIddm(?Dm $iddm): self
    {
        $this->iddm = $iddm;

        return $this;
    }

    public function getIdsalle(): ?Salle
    {
        return $this->idsalle;
    }

    public function setIdsalle(?Salle $idsalle): self
    {
        $this->idsalle = $idsalle;

        return $this;
    }


}
