<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Objet
 *
 * @ORM\Table(name="OBJET")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ObjetRepository")
 */
class Objet
{
    /**
     * @var int
     *
     * @ORM\Column(name="idobjet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idobjet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=200, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomobjet", type="string", length=500, nullable=true)
     */
    private $nomobjet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imageobjet", type="string", length=1000, nullable=true)
     */
    private $imageobjet;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Salle", mappedBy="idobjet")
     */
    private $idsalle;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Objet", inversedBy="idobjetsuperieur")
     * @ORM\JoinTable(name="IS_DANS_OBJET",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idobjetSuperieur", referencedColumnName="idobjet")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idobjetInferieur", referencedColumnName="idobjet")
     *   }
     * )
     */
    private $idobjetinferieur;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idsalle = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idobjetinferieur = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdobjet(): ?int
    {
        return $this->idobjet;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNomobjet(): ?string
    {
        return $this->nomobjet;
    }

    public function setNomobjet(?string $nomobjet): self
    {
        $this->nomobjet = $nomobjet;

        return $this;
    }

    public function getImageobjet(): ?string
    {
        return $this->imageobjet;
    }

    public function setImageobjet(?string $imageobjet): self
    {
        $this->imageobjet = $imageobjet;

        return $this;
    }

    /**
     * @return Collection|Salle[]
     */
    public function getIdsalle(): Collection
    {
        return $this->idsalle;
    }

    public function addIdsalle(Salle $idsalle): self
    {
        if (!$this->idsalle->contains($idsalle)) {
            $this->idsalle[] = $idsalle;
            $idsalle->addIdobjet($this);
        }

        return $this;
    }

    public function removeIdsalle(Salle $idsalle): self
    {
        if ($this->idsalle->contains($idsalle)) {
            $this->idsalle->removeElement($idsalle);
            $idsalle->removeIdobjet($this);
        }

        return $this;
    }

    /**
     * @return Collection|Objet[]
     */
    public function getIdobjetinferieur(): Collection
    {
        return $this->idobjetinferieur;
    }

    public function addIdobjetinferieur(Objet $idobjetinferieur): self
    {
        if (!$this->idobjetinferieur->contains($idobjetinferieur)) {
            $this->idobjetinferieur[] = $idobjetinferieur;
        }

        return $this;
    }

    public function removeIdobjetinferieur(Objet $idobjetinferieur): self
    {
        if ($this->idobjetinferieur->contains($idobjetinferieur)) {
            $this->idobjetinferieur->removeElement($idobjetinferieur);
        }

        return $this;
    }

}
