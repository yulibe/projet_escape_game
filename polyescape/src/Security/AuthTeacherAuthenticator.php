<?php

namespace App\Security;

use App\Entity\Personne;
use App\Entity\Professeur;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Classe permettant l'authentification d'un enseignant
 */
class AuthTeacherAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $entityManager;
    private $router;
    private $csrfTokenManager;

    /**
     * Constructeur de la classe
     * 
     * Attributs:
     *      entityManager = Variable permettant de gerer un entité
     *      router = Gestion des routes
     *      csrfTokenManager = Gestion des token CSRF
     */
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * Methode permettant le supports
     */
    public function supports(Request $request)
    {
        return 'login_prof' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * Recuperation des informations entrées
     */
    public function getCredentials(Request $request)
    {
        $credentials = [
            'emailProf' => $request->request->get('emailProf'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['emailProf']
        );

        return $credentials;
    }

    /**
     * Recuperation des informations utilisateurs
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $perso = $this->entityManager->getRepository(Personne::class)->findOneBy(['email' => $credentials['emailProf']]);

        if (!$perso) {
            // Erreur lors de l'authentification
            throw new CustomUserMessageAuthenticationException('Professeur introuvable');
        }

        $user = $this->entityManager->getRepository(Professeur::class)->findOneBy(['idpers' => $perso->getIdpers()]);

        return $user;
    }

    /**
     * Verification des informations saisies
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $user->AuthValid($credentials);
    }

    /**
     * Fonction lorsque l'authentification est un succés
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        // Redirection vers la page enseignant
        throw new \Exception($this->router->generate('enseignant'));
    }

    /**
     * Recuperation du lien de la page de login
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('login_prof');
    }
}
