<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\Eleve;
use App\Entity\Professeur;

use App\Repository\PersonneRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $form = $this->createFormBuilder()
            -> add('numeroAuth', TextType::class, array("attr" => array("class" => "form-control")))
            -> add('pass', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('save', SubmitType::class, array("label" => "Valider", "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $rep = $this->getDoctrine()->getRepository(Personne::class);
            $verifStudent = $rep->StudentAccountVerification($form->get('numeroAuth')->getData(),sha1($form->get('pass')->getData()));
            $verifTeacher = $rep->TeacherAccountVerification($form->get('numeroAuth')->getData(),sha1($form->get('pass')->getData()));
            
            if(count($verifStudent)>0)
            {

                return new Response("Vous êtes un etudiant, et connecter !");    
            }
            else if(count($verifTeacher)>0)
            {
                return new Response("Vous êtes un professeur, et connecter !");
            }
            else 
            {
                return new Response("Echec !");
            }
        }

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
        ]);
    }
}
