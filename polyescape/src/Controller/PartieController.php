<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PartieController extends AbstractController
{
    /**
     * @Route("/partie", name="salleActuel")
     */
    public function index()
    {
        return $this->render('partie/index.html.twig', [
            'controller_name' => 'PartieController',
        ]);
    }

    /**
     * @Route("/partie/objet", name="objetActuel")
     */
    public function objet()
    {
        return 0;
    }

    /**
     * @Route("/partie/cadenas", name="cadenasActuel")
     */
    public function cadenas()
    {
        return 0;
    }

    /**
     * @Route("/partie/notes/update", name="modifNote")
     */
    public function note()
    {
        return 0;
    }
}
