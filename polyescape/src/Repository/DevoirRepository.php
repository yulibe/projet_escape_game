<?php

namespace App\Repository;

use App\Entity\Devoir;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DevoirRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Devoir::class);
    }

    public function getDevoirParNetu($idEtudiant): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddm, iddev, statut, date, nommat
        FROM ELEVE natural join APPARTIENT natural join GROUPE natural join DM natural join DEVOIR natural join MATIERE
        WHERE netu = :idEtudiant
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idEtudiant' => $idEtudiant]);

        return $stmt->fetchAll();
    }

    public function getDevoirParIdProf($idprof): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, statut, date, nommat,nomdev,nbpersonnegroupe
        FROM PROFESSEUR natural join DEVOIR natural join MATIERE
        WHERE idprof = :idprof
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idprof' => $idprof]);

        return $stmt->fetchAll();
    }
    
    /**
     * Trouver le nombre essai max pour un devoir
     */    
        public function getDevoirNbEssaiMax($iddevoir): int
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT nbessaimax
        FROM DM natural join DEVOIR natural join CADENA natural join CONTIENT
        WHERE iddev = :iddev
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddev' => $iddevoir]);

        return $stmt->fetchAll()[0]['nbessaimax'];
    }
    
      //~ public function getDevoirSupprimer($idDevoir): void
    //~ {
        //~ $conn = $this->getEntityManager()->getConnection();

		//~ $sql = '
        //~ delete from CADANA
        //~ WHERE idcadena  in (select idcadena from CONTIENT natural join DM  where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);
        

        //~ $sql = '
        //~ delete from CONTIENT
        //~ WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);

   
        //~ $sql = '
        //~ delete from EST_LIER_A
        //~ WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);
          
    //~ }
    
    
    /**
     * Supprimer les dm d'un devoir et ses dépendances 
     */
          public function getDevoirSupprimer($idDevoir): void
    {
	 
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
        delete from CONTIENT
        WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idDevoir' => $idDevoir]);

		$sql = '
        delete from CADENA
        WHERE idcadena  not in (select idcadena from CONTIENT);
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idDevoir' => $idDevoir]);
        
   
        $sql = '
        delete from EST_LIER_A
        WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idDevoir' => $idDevoir]);
        
        
        $sql = '
        delete from COMPOSER_DE
        WHERE iddev= :idDevoir;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idDevoir' => $idDevoir]);
        
        $sql = '
        delete from EST_DANS
        WHERE iddev= :idDevoir;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idDevoir' => $idDevoir]);
        
       
         }
    /**
     * Supprimer un dm et ses dépendances 
     */        
     public function getDevoirSupprimerUnDM($iddm): void
    {
	
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
        delete from CONTIENT
        WHERE iddm =:iddm
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddm' => $iddm]);

		$sql = '
        delete from CADENA
        WHERE idcadena  not in (select idcadena from CONTIENT);
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute([]);
        
   
        $sql = '
        delete from EST_LIER_A
        WHERE iddm=:iddm;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddm' => $iddm]);
        
        
        $sql = '
        delete from DM
        WHERE iddm= :iddm;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddm' => $iddm]);
     
         }
      
      
           //~ public function getDevoirSupprimer($idDevoir): void
    //~ {
        //~ $conn = $this->getEntityManager()->getConnection();

		//~ $sql = '
        //~ delete from CADANA
        //~ WHERE idcadena  in (select idcadena from CONTIENT natural join DM  where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);
        

        //~ $sql = '
        //~ delete from CONTIENT
        //~ WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);

   
        //~ $sql = '
        //~ delete from EST_LIER_A
        //~ WHERE iddm  in (select iddm from DM where iddev= :idDevoir);
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);
        
       
        
    //~ }
    //~ public function getDevoirSupprimer($idDevoir): void
    //~ {
        //~ $sql = '
        //~ select * from COMPOSER_DE
        //~ WHERE iddev= :idDevoir;
            //~ ';
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idDevoir' => $idDevoir]);
        //~ $listeSalle=$stmt->fetchAll();
        //~ foreach ($listeSalle as $salle){
			//~ $sql1 = '
			//~ select * from IS_DANS
			//~ WHERE idsalle= :idsalle;
				//~ ';
			//~ $stmt1 = $conn->prepare($sql1);
			//~ $stmt1->execute(['idsalle' => $salle]);
			//~ $listeSalle=$stmt1->fetchAll();
			
			//~ }
       
        
    //~ }
    
    

    public function getclassementEleve($iddev): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, nom, prenom, classe, note, netu,idgr,iddm
        FROM DEVOIR natural join ELEVE natural join DM natural join PERSONNE natural join GROUPE natural join APPARTIENT
        WHERE iddev = :iddev
        ORDER BY note,idgr DESC
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddev' => $iddev]);

        return $stmt->fetchAll();
    }
    public function changerOrdersalle($idsalle ,$idevoir,$ordersalle): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        UPDATE COMPOSER_DE
		SET ordresalle = :ordersalle
		WHERE idsalle=:idsalle
		and iddev=:idevoir;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idsalle' =>$idsalle,'idevoir'=>$idevoir,'ordersalle'=>$ordersalle]);

    }


    /**
     * Trouver le nombre de point d'un devoir
     */    
        public function NbPoint($idevoir): int
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT sum(maxpoint) as pt
        FROM `EST_DANS` NATURAL JOIN ENIGME 
        where iddev=:idevoir;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idevoir'=>$idevoir]);
	return intval($stmt->fetchAll()[0]['pt']);
    }

    public function getInfosDev($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, date, nbpersonnegroupe, nbgroupe, statut, nomDev, nommat
        FROM DEVOIR natural join MATIERE
        where iddev=:iddevoir;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' => $iddevoir]);

        return $stmt->fetchAll();
    }

    public function getEnigmesDevoir($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, nomEnigme, question, solution, niveau
        FROM `EST_DANS` NATURAL JOIN ENIGME 
        where iddev=:iddevoir;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' => $iddevoir]);

        return $stmt->fetchAll();
    }

    public function getGroupesDev($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, nomGroupe, nom, prenom
        FROM DEVOIR natural join DM natural join GROUPE natural join ELEVE natural join PERSONNE
        where iddev=:iddevoir and GROUPE.idpush=ELEVE.netu;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' => $iddevoir]);

        return $stmt->fetchAll();
    }

    public function getSallesDev($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddev, idsalle, nomsalle, descriptsalle
        FROM COMPOSER_DE natural join SALLE
        where iddev=:iddevoir;
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' => $iddevoir]);

        return $stmt->fetchAll();
    }

}
