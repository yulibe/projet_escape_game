<?php

namespace App\Repository;

use App\Entity\Dm;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dm::class);
    }

    public function getGroupMarkForStudent($idEtudiant): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM ELEVE natural join APPARTIENT natural join GROUPE natural join DM
        WHERE netu = :idEtudiant
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idEtudiant' => $idEtudiant]);

        return $stmt->fetchAll();
    }

    public function getStudentByGroup($idGroupe): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT idgr, nom, prenom
        FROM GROUPE natural join APPARTIENT natural join ELEVE natural join PERSONNE
        WHERE idgr = :idGroupe 
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idGroupe' => $idGroupe]);

        return $stmt->fetchAll();
    }

    public function getDMParNetu($idEtudiant): array
    {
        /**
        * SELECT DISTINCT iddm, iddev, statut, date, nommat
        * FROM APPARTIENT NATURAL JOIN DM NATURAL JOIN DEVOIR NATURAL JOIN MATIERE
        * WHERE netu = 100
        */

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT iddm, iddev, statut, date, nommat,nomdev
        FROM APPARTIENT NATURAL JOIN DM NATURAL JOIN DEVOIR NATURAL JOIN MATIERE
        WHERE netu = :idEtudiant
        ORDER BY statut
            ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idEtudiant' => $idEtudiant]);

        return $stmt->fetchAll();
    }
   
}
