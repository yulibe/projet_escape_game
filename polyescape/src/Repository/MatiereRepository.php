<?php

namespace App\Repository;

use App\Entity\Matiere;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MatiereRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Matiere::class);
    }

    public function removeMatiere($idMatiere): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE 
            FROM MATIERE
                WHERE idmat = :idMatiere and 
                                            :idMatiere not in(select idmat FROM ENIGME);
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idMatiere' => $idMatiere,
        ]);
    }
    

}
