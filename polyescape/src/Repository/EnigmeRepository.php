<?php

namespace App\Repository;

use App\Entity\Enigme;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EnigmeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Enigme::class);
    }

    public function removeEnigme($idEnigme): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE 
            FROM ENIGME 
                WHERE idenigme = :idEnigme and 
                                            :idEnigme not in(select idEnigme FROM EST_DANS);
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idEnigme' => $idEnigme,
        ]);
    }
    public function removeMatiere($idMat){
        
    }

}
