-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 08 Décembre 2018 à 21:27
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.32-2+0~20181015120817.7+stretch~1.gbpa6b8cf

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ESCAPE`
--

-- --------------------------------------------------------

--
-- Structure de la table `APPARTIENT`
--

DROP TABLE IF EXISTS `APPARTIENT`;
CREATE TABLE IF NOT EXISTS `APPARTIENT` (
  `idgr` int(11) NOT NULL,
  `netu` int(11) NOT NULL,
  PRIMARY KEY (`idgr`,`netu`),
  KEY `netu` (`netu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `APPARTIENT`
--

INSERT INTO `APPARTIENT` (`idgr`, `netu`) VALUES
(1, 45646),
(1, 54987),
(1, 265689),
(1, 458864),
(1, 564564),
(1, 2171927);

-- --------------------------------------------------------

--
-- Structure de la table `CADENA`
--

DROP TABLE IF EXISTS `CADENA`;
CREATE TABLE IF NOT EXISTS `CADENA` (
  `idcadena` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` text,
  `niveauc` int(11) DEFAULT NULL,
  `etatc` tinyint(1) DEFAULT NULL,
  `nbessai` int(11) DEFAULT NULL,
  `nbessaimax` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcadena`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CADENA`
--

INSERT INTO `CADENA` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(1, 'Première pièce ', 5, 0, 0, 10);

-- --------------------------------------------------------

--
-- Structure de la table `CONTIENT`
--

DROP TABLE IF EXISTS `CONTIENT`;
CREATE TABLE IF NOT EXISTS `CONTIENT` (
  `iddm` int(11) NOT NULL,
  `idcadena` int(11) NOT NULL,
  `idsalle` int(11) NOT NULL,
  PRIMARY KEY (`iddm`,`idcadena`,`idsalle`),
  KEY `idsalle` (`idsalle`),
  KEY `idcadena` (`idcadena`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CONTIENT`
--

INSERT INTO `CONTIENT` (`iddm`, `idcadena`, `idsalle`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `DEVOIR`
--

DROP TABLE IF EXISTS `DEVOIR`;
CREATE TABLE IF NOT EXISTS `DEVOIR` (
  `iddev` int(11) NOT NULL AUTO_INCREMENT,
  `statut` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nbpersonnegroupe` int(11) DEFAULT NULL,
  `nbgroupe` int(11) DEFAULT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idmat` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddev`),
  KEY `idmat` (`idmat`),
  KEY `idprof` (`idprof`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `DEVOIR`
--

INSERT INTO `DEVOIR` (`iddev`, `statut`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`) VALUES
(1, 'En developpement', '2018-12-08', 4, 10, 2221, 1);

-- --------------------------------------------------------

--
-- Structure de la table `DM`
--

DROP TABLE IF EXISTS `DM`;
CREATE TABLE IF NOT EXISTS `DM` (
  `iddm` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(8) DEFAULT NULL,
  `idgr` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddm`),
  KEY `iddev` (`iddev`),
  KEY `idgr` (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `DM`
--

INSERT INTO `DM` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(1, '20/20', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ELEVE`
--

DROP TABLE IF EXISTS `ELEVE`;
CREATE TABLE IF NOT EXISTS `ELEVE` (
  `netu` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(20) DEFAULT NULL,
  `datec` varchar(10) DEFAULT NULL,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`netu`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=2171928 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ELEVE`
--

INSERT INTO `ELEVE` (`netu`, `classe`, `datec`, `idpers`) VALUES
(45646, '2A21', '2018-2019', 7),
(54987, '2A21', '2018-2019', 4),
(265689, '2A21', '2018-2019', 5),
(458864, '2A21', '2018-2019', 3),
(564564, '2A21', '2018-2019', 6),
(2171927, '2A21', '2018-2019', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ENIGME`
--

DROP TABLE IF EXISTS `ENIGME`;
CREATE TABLE IF NOT EXISTS `ENIGME` (
  `idenigme` int(11) NOT NULL AUTO_INCREMENT,
  `solution` text,
  `niveau` int(11) DEFAULT NULL,
  `maxpoint` int(11) DEFAULT NULL,
  `question` text,
  `idmat` int(11) DEFAULT NULL,
  `niveau_1` int(11) DEFAULT NULL,
  PRIMARY KEY (`idenigme`),
  KEY `niveau_1` (`niveau_1`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ENIGME`
--

INSERT INTO `ENIGME` (`idenigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`, `niveau_1`) VALUES
(1, '2 + 2 = 4', 3, 5, '2 + 2 = ?', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ENSEIGNE`
--

DROP TABLE IF EXISTS `ENSEIGNE`;
CREATE TABLE IF NOT EXISTS `ENSEIGNE` (
  `idprof` int(11) NOT NULL,
  `idmat` int(11) NOT NULL,
  PRIMARY KEY (`idprof`,`idmat`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ENSEIGNE`
--

INSERT INTO `ENSEIGNE` (`idprof`, `idmat`) VALUES
(2221, 1),
(11452, 4);

-- --------------------------------------------------------

--
-- Structure de la table `EST_DANS`
--

DROP TABLE IF EXISTS `EST_DANS`;
CREATE TABLE IF NOT EXISTS `EST_DANS` (
  `idenigme` int(11) NOT NULL,
  `iddev` int(11) NOT NULL,
  PRIMARY KEY (`idenigme`,`iddev`),
  KEY `iddev` (`iddev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `EST_DANS`
--

INSERT INTO `EST_DANS` (`idenigme`, `iddev`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `EST_LIER_A`
--

DROP TABLE IF EXISTS `EST_LIER_A`;
CREATE TABLE IF NOT EXISTS `EST_LIER_A` (
  `iddm` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `idenigme` int(11) NOT NULL,
  `datetrouve` date DEFAULT NULL,
  PRIMARY KEY (`iddm`,`idobjet`,`idenigme`),
  KEY `idenigme` (`idenigme`),
  KEY `idobjet` (`idobjet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `EST_LIER_A`
--

INSERT INTO `EST_LIER_A` (`iddm`, `idobjet`, `idenigme`, `datetrouve`) VALUES
(1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `GROUPE`
--

DROP TABLE IF EXISTS `GROUPE`;
CREATE TABLE IF NOT EXISTS `GROUPE` (
  `idgr` int(11) NOT NULL AUTO_INCREMENT,
  `idpush` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `GROUPE`
--

INSERT INTO `GROUPE` (`idgr`, `idpush`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `IS_DANS`
--

DROP TABLE IF EXISTS `IS_DANS`;
CREATE TABLE IF NOT EXISTS `IS_DANS` (
  `idsalle` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `coordobjet` varchar(200) DEFAULT NULL,
  `tailleobjet` int(11) DEFAULT NULL,
  PRIMARY KEY (`idobjet`,`idsalle`),
  KEY `idsalle` (`idsalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `IS_DANS`
--

INSERT INTO `IS_DANS` (`idsalle`, `idobjet`, `coordobjet`, `tailleobjet`) VALUES
(1, 1, '(52;14)', 2);

-- --------------------------------------------------------

--
-- Structure de la table `IS_DANS_OBJET`
--

DROP TABLE IF EXISTS `IS_DANS_OBJET`;
CREATE TABLE IF NOT EXISTS `IS_DANS_OBJET` (
  `idobjetSuperieur` int(11) NOT NULL,
  `idobjetInferieur` int(11) NOT NULL,
  PRIMARY KEY (`idobjetSuperieur`,`idobjetInferieur`),
  KEY `idobjetInferieur` (`idobjetInferieur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `IS_DANS_OBJET`
--

INSERT INTO `IS_DANS_OBJET` (`idobjetSuperieur`, `idobjetInferieur`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `MATIERE`
--

DROP TABLE IF EXISTS `MATIERE`;
CREATE TABLE IF NOT EXISTS `MATIERE` (
  `idmat` int(11) NOT NULL AUTO_INCREMENT,
  `nommat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MATIERE`
--

INSERT INTO `MATIERE` (`idmat`, `nommat`) VALUES
(1, 'Math'),
(2, 'SVT'),
(3, 'Physique'),
(4, 'Informatique');

-- --------------------------------------------------------

--
-- Structure de la table `NIVEAU`
--

DROP TABLE IF EXISTS `NIVEAU`;
CREATE TABLE IF NOT EXISTS `NIVEAU` (
  `niveau` int(11) NOT NULL,
  `descriptionniveau` text,
  PRIMARY KEY (`niveau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `NIVEAU`
--

INSERT INTO `NIVEAU` (`niveau`, `descriptionniveau`) VALUES
(1, 'Le premier niveau'),
(2, 'Le second niveau');

-- --------------------------------------------------------

--
-- Structure de la table `OBJET`
--

DROP TABLE IF EXISTS `OBJET`;
CREATE TABLE IF NOT EXISTS `OBJET` (
  `idobjet` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) DEFAULT NULL,
  `nomobjet` varchar(500) DEFAULT NULL,
  `imageobjet` blob,
  PRIMARY KEY (`idobjet`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `OBJET`
--

INSERT INTO `OBJET` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(1, 'Table', 'Table ronde', NULL),
(2, 'Lit', 'Matelas en mauvais état', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PERSONNE`
--

DROP TABLE IF EXISTS `PERSONNE`;
CREATE TABLE IF NOT EXISTS `PERSONNE` (
  `idpers` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PERSONNE`
--

INSERT INTO `PERSONNE` (`idpers`, `nom`, `prenom`, `mdp`, `email`) VALUES
(1, 'Aubry', 'Erwan', '59c826fc854197cbd4d1083bce8fc00d0761e8b3', 'geekyready@gmail.com'),
(2, 'Kalla', 'Caroline', 'a020e408cf13b0b1e054fd9168ee17cb291300f0', 'caroline.kalla@univ-orleans.fr'),
(3, 'Jurè', 'Marion', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'marion.jure@etu.univ-orleans.fr'),
(4, 'Cizeau', 'Valentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'valentin.cizeau@etu.univ-orleans.fr'),
(5, 'Delaunay', 'Julien', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'julien.delaunay@etu.univ-orleans.fr'),
(6, 'Mercier', 'Luka', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'luka.mercier@etu.univ-orleans.fr'),
(7, 'Chateigner', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(8, 'Cleuziou', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr');

-- --------------------------------------------------------

--
-- Structure de la table `PROFESSEUR`
--

DROP TABLE IF EXISTS `PROFESSEUR`;
CREATE TABLE IF NOT EXISTS `PROFESSEUR` (
  `idprof` int(11) NOT NULL AUTO_INCREMENT,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`idprof`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=11453 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PROFESSEUR`
--

INSERT INTO `PROFESSEUR` (`idprof`, `idpers`) VALUES
(11452, 2),
(2221, 8);

-- --------------------------------------------------------

--
-- Structure de la table `SALLE`
--

DROP TABLE IF EXISTS `SALLE`;
CREATE TABLE IF NOT EXISTS `SALLE` (
  `idsalle` int(11) NOT NULL AUTO_INCREMENT,
  `nomsalle` varchar(500) DEFAULT NULL,
  `descriptsalle` text,
  `imagesalle` blob,
  `niveau` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL,
  `ordresalle` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idsalle`),
  KEY `iddev` (`iddev`),
  KEY `niveau` (`niveau`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SALLE`
--

INSERT INTO `SALLE` (`idsalle`, `nomsalle`, `descriptsalle`, `imagesalle`, `niveau`, `iddev`, `ordresalle`) VALUES
(1, 'La chambre', 'Une chambre', NULL, 1, 1, '1');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `APPARTIENT`
--
ALTER TABLE `APPARTIENT`
  ADD CONSTRAINT `APPARTIENT_ibfk_1` FOREIGN KEY (`netu`) REFERENCES `ELEVE` (`netu`),
  ADD CONSTRAINT `APPARTIENT_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

--
-- Contraintes pour la table `CONTIENT`
--
ALTER TABLE `CONTIENT`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idcadena`) REFERENCES `CADENA` (`idcadena`),
  ADD CONSTRAINT `CONTIENT_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

--
-- Contraintes pour la table `DEVOIR`
--
ALTER TABLE `DEVOIR`
  ADD CONSTRAINT `DEVOIR_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `DEVOIR_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

--
-- Contraintes pour la table `DM`
--
ALTER TABLE `DM`
  ADD CONSTRAINT `DM_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `DM_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

--
-- Contraintes pour la table `ELEVE`
--
ALTER TABLE `ELEVE`
  ADD CONSTRAINT `ELEVE_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

--
-- Contraintes pour la table `ENIGME`
--
ALTER TABLE `ENIGME`
  ADD CONSTRAINT `ENIGME_ibfk_1` FOREIGN KEY (`niveau_1`) REFERENCES `NIVEAU` (`niveau`),
  ADD CONSTRAINT `ENIGME_ibfk_2` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`);

--
-- Contraintes pour la table `ENSEIGNE`
--
ALTER TABLE `ENSEIGNE`
  ADD CONSTRAINT `ENSEIGNE_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `ENSEIGNE_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

--
-- Contraintes pour la table `EST_DANS`
--
ALTER TABLE `EST_DANS`
  ADD CONSTRAINT `EST_DANS_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `EST_DANS_ibfk_2` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`);

--
-- Contraintes pour la table `EST_LIER_A`
--
ALTER TABLE `EST_LIER_A`
  ADD CONSTRAINT `EST_LIER_A_ibfk_1` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_2` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

--
-- Contraintes pour la table `IS_DANS`
--
ALTER TABLE `IS_DANS`
  ADD CONSTRAINT `IS_DANS_ibfk_1` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

--
-- Contraintes pour la table `IS_DANS_OBJET`
--
ALTER TABLE `IS_DANS_OBJET`
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_1` FOREIGN KEY (`idobjetSuperieur`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_2` FOREIGN KEY (`idobjetInferieur`) REFERENCES `OBJET` (`idobjet`);

--
-- Contraintes pour la table `PROFESSEUR`
--
ALTER TABLE `PROFESSEUR`
  ADD CONSTRAINT `PROFESSEUR_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

--
-- Contraintes pour la table `SALLE`
--
ALTER TABLE `SALLE`
  ADD CONSTRAINT `SALLE_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `SALLE_ibfk_2` FOREIGN KEY (`niveau`) REFERENCES `NIVEAU` (`niveau`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
