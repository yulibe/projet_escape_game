-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 08 Décembre 2018 à 21:27
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.32-2+0~20181015120817.7+stretch~1.gbpa6b8cf

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ESCAPE`
--

-- --------------------------------------------------------

--
-- DROP des tables
--
DROP TABLE IF EXISTS APPARTIENT;
DROP TABLE IF EXISTS COMPOSER_DE;
DROP TABLE IF EXISTS CONTIENT;
DROP TABLE IF EXISTS CADENA;
DROP TABLE IF EXISTS ELEVE;
DROP TABLE IF EXISTS ENSEIGNE;
DROP TABLE IF EXISTS EST_DANS;
DROP TABLE IF EXISTS EST_LIER_A;
DROP TABLE IF EXISTS IS_DANS;
DROP TABLE IF EXISTS IS_DANS_OBJET;
DROP TABLE IF EXISTS DM;
DROP TABLE IF EXISTS DEVOIR;
DROP TABLE IF EXISTS ENIGME;
DROP TABLE IF EXISTS GROUPE;
DROP TABLE IF EXISTS MATIERE;
DROP TABLE IF EXISTS NIVEAU;
DROP TABLE IF EXISTS OBJET;
DROP TABLE IF EXISTS PROFESSEUR;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS SALLE;


-- --------------------------------------------------------

--
-- Creation des tables
--

CREATE TABLE IF NOT EXISTS `APPARTIENT` (
  `idgr` int(11) NOT NULL,
  `netu` int(11) NOT NULL,
  PRIMARY KEY (`idgr`,`netu`),
  KEY `netu` (`netu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CADENA` (
  `idcadena` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` text,
  `niveauc` int(11) DEFAULT NULL,
  `etatc` tinyint(1) DEFAULT NULL,
  `nbessai` int(11) DEFAULT NULL,
  `nbessaimax` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcadena`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `CONTIENT` (
  `iddm` int(11) NOT NULL,
  `idcadena` int(11) NOT NULL,
  `idsalle` int(11) NOT NULL,
  PRIMARY KEY (`iddm`,`idcadena`,`idsalle`),
  KEY `idsalle` (`idsalle`),
  KEY `idcadena` (`idcadena`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DEVOIR` (
  `iddev` int(11) NOT NULL AUTO_INCREMENT,
  `statut` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nbpersonnegroupe` int(11) DEFAULT NULL,
  `nbgroupe` int(11) DEFAULT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idmat` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddev`),
  KEY `idmat` (`idmat`),
  KEY `idprof` (`idprof`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `DM` (
  `iddm` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(8) DEFAULT NULL,
  `idgr` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddm`),
  KEY `iddev` (`iddev`),
  KEY `idgr` (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `ELEVE` (
  `netu` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(20) DEFAULT NULL,
  `datec` varchar(10) DEFAULT NULL,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`netu`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=2171928 DEFAULT CHARSET=utf8;

CREATE TABLE `ENIGME` (
  `idenigme` int(11) NOT NULL AUTO_INCREMENT,
  `nomEnigme` varchar(30),
  `solution` text,
  `niveau` int(11) DEFAULT NULL,
  `maxpoint` int(11) DEFAULT NULL,
  `question` text,
  `idmat` int(11) DEFAULT NULL,
  PRIMARY KEY (`idenigme`),
  KEY `niveau` (`niveau`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `ENSEIGNE` (
  `idprof` int(11) NOT NULL,
  `idmat` int(11) NOT NULL,
  PRIMARY KEY (`idprof`,`idmat`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_DANS` (
  `idenigme` int(11) NOT NULL,
  `iddev` int(11) NOT NULL,
  PRIMARY KEY (`idenigme`,`iddev`),
  KEY `iddev` (`iddev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_LIER_A` (
  `iddm` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `idenigme` int(11) NOT NULL,
  `datetrouve` date DEFAULT NULL,
  PRIMARY KEY (`iddm`,`idobjet`,`idenigme`),
  KEY `idenigme` (`idenigme`),
  KEY `idobjet` (`idobjet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `GROUPE` (
  `idgr` int(11) NOT NULL AUTO_INCREMENT,
  `nomGroupe` varchar(100) DEFAULT NULL,
  `idpush` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS` (
  `idsalle` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `coordobjet` varchar(200) DEFAULT NULL,
  `tailleobjet` int(11) DEFAULT NULL,
  PRIMARY KEY (`idobjet`,`idsalle`),
  KEY `idsalle` (`idsalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS_OBJET` (
  `idobjetSuperieur` int(11) NOT NULL,
  `idobjetInferieur` int(11) NOT NULL,
  `coordobjet2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idobjetSuperieur`,`idobjetInferieur`),
  KEY `idobjetInferieur` (`idobjetInferieur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MATIERE` (
  `idmat` int(11) NOT NULL AUTO_INCREMENT,
  `nommat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `NIVEAU` (
  `niveau` int(11) NOT NULL,
  `descriptionniveau` text,
  PRIMARY KEY (`niveau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `OBJET` (
  `idobjet` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) DEFAULT NULL,
  `nomobjet` varchar(500) DEFAULT NULL,
  `imageobjet` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idobjet`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `PERSONNE` (
  `idpers` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `PROFESSEUR` (
  `idprof` int(11) NOT NULL AUTO_INCREMENT,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`idprof`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=11453 DEFAULT CHARSET=utf8;

CREATE TABLE `SALLE` (
  `idsalle` int(11) NOT NULL AUTO_INCREMENT,
  `nomsalle` varchar(500) DEFAULT NULL,
  `descriptsalle` text,
  `imagesalle` varchar(1000),
  PRIMARY KEY (`idsalle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `COMPOSER_DE` (
  `idsalle` int(11) NOT NULL AUTO_INCREMENT,
  `iddev` int(11) DEFAULT NULL,
  `ordresalle` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idsalle`,`iddev` ),
  KEY `iddev` (`iddev`),
  KEY `idsalle` (`idsalle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Insertion des tables
--
INSERT INTO `DEVOIR` (`iddev`, `statut`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`) VALUES
(3, 'En cours', '2018-12-08', 4, 10, 2221, 1);

INSERT INTO `GROUPE` (`idgr`,`nomGroupe` , `idpush`) VALUES
(2,'groupe 2',1),
(3,'groupe 2',1),
(4,'groupe 2',1);


INSERT INTO `DM` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(3, '20/20', 2, 3),
(4, '20/20', 3, 3),
(5, '20/20', 4, 3);

INSERT INTO `ENIGME` (`idenigme`, `nomEnigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`) VALUES
(2,'E2','2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(3,'E3','2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(4,'E4','2 + 2 = 4', 2, 5, '2 + 2 = ?', 1)
;




INSERT INTO `OBJET` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(4, 'objet', 'Matelas en mauvais état', NULL);


INSERT INTO `EST_DANS` (`idenigme`, `iddev`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3);

INSERT INTO `COMPOSER_DE`  (`idsalle`, `iddev`, `ordresalle`) VALUES
(1, 3, '1'),
(2, 3, '1');


INSERT INTO `EST_LIER_A` (`iddm`, `idobjet`, `idenigme`, `datetrouve`) VALUES
(3, 1, 1, NULL),
(3, 2, 2, NULL),
(4, 1, 1, NULL),
(4, 2, 2, NULL),
(5, 1, 1, NULL),
(5, 2, 2, NULL);

INSERT INTO `CADENA` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(2, 'C2', 5, 0, 0, 10),
(3, 'C3 ', 5, 0, 0, 10),
(4, 'C4', 5, 0, 0, 10),
(5, 'C5', 5, 0, 0, 10),
(6, 'C6', 5, 0, 0, 10);


INSERT INTO `CONTIENT` (`iddm`, `idcadena`, `idsalle`) VALUES
(3, 1, 1),
(3, 2, 2),
(4, 3, 1),
(4, 4, 2),
(5, 5, 1),
(5, 6, 2);







INSERT INTO `APPARTIENT` (`idgr`, `netu`) VALUES
(1, 45646),
(1, 54987),
(1, 265689),
(1, 458864),
(1, 564564),
(1, 2171927);

INSERT INTO `CADENA` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(1, 'Première pièce ', 5, 0, 0, 10);

INSERT INTO `CONTIENT` (`iddm`, `idcadena`, `idsalle`) VALUES
(1, 1, 1);

INSERT INTO `DEVOIR` (`iddev`, `statut`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`) VALUES
(1, 'En developpement', '2018-12-08', 4, 10, 2221, 1);

INSERT INTO `DM` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(1, '20/20', 1, 1);

INSERT INTO `ELEVE` (`netu`, `classe`, `datec`, `idpers`) VALUES

(222, '2A22', '2018-2019', 12),
(333, '2A22', '2018-2019', 10),
(444, '2A22', '2018-2019', 11);

INSERT INTO `ENIGME` (`idenigme`, `nomEnigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`) VALUES
(2,'E1','5', 2, 5, '2 + 3 = ?', 1),
(3,'E2','6', 2, 5, '3 + 3 = ?', 1),
(4,'E3','1', 2, 5, '1 + 0 = ?', 1),
(5,'E4','14', 2, 5, '2 + 12 = ?', 1);

INSERT INTO `ENSEIGNE` (`idprof`, `idmat`) VALUES
(2221, 1),
(11452, 4);

INSERT INTO `EST_DANS` (`idenigme`, `iddev`) VALUES
(2,1),
(4,1),
(3,1);

INSERT INTO `EST_LIER_A` (`iddm`, `idobjet`, `idenigme`, `datetrouve`) VALUES
(1, 1, 1, NULL);

INSERT INTO `GROUPE` (`idgr`,`nomGroupe` , `idpush`) VALUES
(1,'groupe 1',1),
(2,'groupe 2',2),
(3,'groupe 3',2),
(4,'groupe 4',3);

INSERT INTO `IS_DANS` (`idsalle`, `idobjet`, `coordobjet`, `tailleobjet`) VALUES
(1, 17, '(10;10;10;10)', 2);


INSERT INTO `OBJET` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(3, 'Objet', 'Table ', NULL),
(17, 'Objet', 'chaise', NULL),
(18, 'Objet', 'ordi ', NULL),
(19, 'Objet', 'livre', NULL);

INSERT INTO `IS_DANS_OBJET` (`idobjetSuperieur`, `idobjetInferieur`, `coordobjet2`) VALUES
(1, 2, '(52;14)');

INSERT INTO `MATIERE` (`idmat`, `nommat`) VALUES
(1, 'Math'),
(2, 'SVT'),
(3, 'Physique'),
(4, 'Informatique');

INSERT INTO `NIVEAU` (`niveau`, `descriptionniveau`) VALUES
(1, 'Le premier niveau'),
(2, 'Le second niveau');



INSERT INTO `PERSONNE` (`idpers`, `nom`, `prenom`, `mdp`, `email`) VALUES
(12, 'Marie', 'Luka', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'luka.mercier@etu.univ-orleans.fr'),
(10, 'Paul', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(11, 'Pierre', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr');

INSERT INTO `PROFESSEUR` (`idprof`, `idpers`) VALUES
(11452, 2),
(2221, 8);

INSERT INTO `SALLE` (`idsalle`, `nomsalle`, `descriptsalle`, `imagesalle`) VALUES
(2, 'La cuisine', 'une cuisine', NULL),
(3, 'Le salon', 'un salon', NULL);

INSERT INTO `COMPOSER_DE`  (`idsalle`, `iddev`, `ordresalle`) VALUES
(2, 1, '2');

-- ~ INSERT INTO `CONTIENT` (`iddm`,`idcadena` ,`idsalle) VALUES
-- ~ (1, 'Première pièce ', 5, 0, 0, 10);

-- --------------------------------------------------------

--
-- Contrainte des tables
--
ALTER TABLE `APPARTIENT`
  ADD CONSTRAINT `APPARTIENT_ibfk_1` FOREIGN KEY (`netu`) REFERENCES `ELEVE` (`netu`),
  ADD CONSTRAINT `APPARTIENT_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `CONTIENT`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idcadena`) REFERENCES `CADENA` (`idcadena`),
  ADD CONSTRAINT `CONTIENT_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

ALTER TABLE `DEVOIR`
  ADD CONSTRAINT `DEVOIR_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `DEVOIR_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `DM`
  ADD CONSTRAINT `DM_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `DM_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `ELEVE`
  ADD CONSTRAINT `ELEVE_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

ALTER TABLE `ENIGME`
  ADD CONSTRAINT `ENIGME_ibfk_1` FOREIGN KEY (`niveau`) REFERENCES `NIVEAU` (`niveau`),
  ADD CONSTRAINT `ENIGME_ibfk_2` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`);

ALTER TABLE `ENSEIGNE`
  ADD CONSTRAINT `ENSEIGNE_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `ENSEIGNE_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `EST_DANS`
  ADD CONSTRAINT `EST_DANS_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `EST_DANS_ibfk_2` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`);

ALTER TABLE `EST_LIER_A`
  ADD CONSTRAINT `EST_LIER_A_ibfk_1` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_2` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

ALTER TABLE `IS_DANS`
  ADD CONSTRAINT `IS_DANS_ibfk_1` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `IS_DANS_OBJET`
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_1` FOREIGN KEY (`idobjetSuperieur`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_2` FOREIGN KEY (`idobjetInferieur`) REFERENCES `OBJET` (`idobjet`);

ALTER TABLE `PROFESSEUR`
  ADD CONSTRAINT `PROFESSEUR_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

ALTER TABLE `COMPOSER_DE`
  ADD CONSTRAINT `COMPOSER_DE_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `COMPOSER_DE_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
