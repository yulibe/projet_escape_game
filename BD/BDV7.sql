-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 08 Décembre 2018 à 21:27
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.32-2+0~20181015120817.7+stretch~1.gbpa6b8cf

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ESCAPE`
--

-- --------------------------------------------------------

--
-- DROP des tables
--
DROP TABLE IF EXISTS APPARTIENT;
DROP TABLE IF EXISTS COMPOSER_DE;
DROP TABLE IF EXISTS CONTIENT;
DROP TABLE IF EXISTS CADENA;
DROP TABLE IF EXISTS ELEVE;
DROP TABLE IF EXISTS ENSEIGNE;
DROP TABLE IF EXISTS EST_DANS;
DROP TABLE IF EXISTS EST_LIER_A;
DROP TABLE IF EXISTS IS_DANS;
DROP TABLE IF EXISTS IS_DANS_OBJET;
DROP TABLE IF EXISTS DM;
DROP TABLE IF EXISTS DEVOIR;
DROP TABLE IF EXISTS ENIGME;
DROP TABLE IF EXISTS GROUPE;
DROP TABLE IF EXISTS MATIERE;
DROP TABLE IF EXISTS NIVEAU;
DROP TABLE IF EXISTS OBJET;
DROP TABLE IF EXISTS PROFESSEUR;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS SALLE;


-- --------------------------------------------------------

--
-- Creation des tables
--

CREATE TABLE IF NOT EXISTS `APPARTIENT` (
  `idgr` int(11) NOT NULL,
  `netu` int(11) NOT NULL,
  PRIMARY KEY (`idgr`,`netu`),
  KEY `netu` (`netu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CADENA` (
  `idcadena` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` text,
  `niveauc` int(11) DEFAULT NULL,
  `etatc` tinyint(1) DEFAULT NULL,
  `nbessai` int(11) DEFAULT NULL,
  `nbessaimax` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcadena`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `CONTIENT` (
  `iddm` int(11) NOT NULL,
  `idcadena` int(11) NOT NULL,
  `idsalle` int(11) NOT NULL,
  PRIMARY KEY (`iddm`,`idcadena`,`idsalle`),
  KEY `idsalle` (`idsalle`),
  KEY `idcadena` (`idcadena`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DEVOIR` (
  `iddev` int(11) NOT NULL AUTO_INCREMENT,
  `statut` varchar(50) DEFAULT NULL,
  `nomDev` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nbpersonnegroupe` int(11) DEFAULT NULL,
  `nbgroupe` int(11) DEFAULT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idmat` int(11) DEFAULT NULL,
  `maxPoint` int(5) DEFAULT 0,
  PRIMARY KEY (`iddev`),
  KEY `idmat` (`idmat`),
  KEY `idprof` (`idprof`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `DM` (
  `iddm` int(11) NOT NULL AUTO_INCREMENT,
  `note` int(5) DEFAULT 0,
  `idgr` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddm`),
  KEY `iddev` (`iddev`),
  KEY `idgr` (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `ELEVE` (
  `netu` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(20) DEFAULT NULL,
  `datec` varchar(10) DEFAULT NULL,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`netu`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=2171928 DEFAULT CHARSET=utf8;

CREATE TABLE `ENIGME` (
  `idenigme` int(11) NOT NULL AUTO_INCREMENT,
  `nomEnigme` varchar(30),
  `solution` text,
  `niveau` int(11) DEFAULT NULL,
  `maxpoint` int(11) DEFAULT 0,
  `question` text,
  `idmat` int(11) DEFAULT NULL,
  PRIMARY KEY (`idenigme`),
  KEY `niveau` (`niveau`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `ENSEIGNE` (
  `idprof` int(11) NOT NULL,
  `idmat` int(11) NOT NULL,
  PRIMARY KEY (`idprof`,`idmat`),
  KEY `idmat` (`idmat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_DANS` (
  `idenigme` int(11) NOT NULL,
  `iddev` int(11) NOT NULL,
  PRIMARY KEY (`idenigme`,`iddev`),
  KEY `iddev` (`iddev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_LIER_A` (
  `iddm` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `idenigme` int(11) NOT NULL,
  `datetrouve` date DEFAULT NULL,
  `idsalle` int(11) NOT NULL,
  PRIMARY KEY (`iddm`,`idobjet`,`idenigme`),
  KEY `idenigme` (`idenigme`),
  KEY `idobjet` (`idobjet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `GROUPE` (
  `idgr` int(11) NOT NULL AUTO_INCREMENT,
  `nomGroupe` varchar(100) DEFAULT NULL,
  `idpush` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS` (
  `idsalle` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `coordobjet` varchar(200) DEFAULT NULL,
  `tailleobjet` int(11) DEFAULT NULL,
  PRIMARY KEY (`idobjet`,`idsalle`),
  KEY `idsalle` (`idsalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS_OBJET` (
  `idobjetSuperieur` int(11) NOT NULL,
  `idobjetInferieur` int(11) NOT NULL,
  `coordobjet2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idobjetSuperieur`,`idobjetInferieur`),
  KEY `idobjetInferieur` (`idobjetInferieur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MATIERE` (
  `idmat` int(11) NOT NULL AUTO_INCREMENT,
  `nommat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `NIVEAU` (
  `niveau` int(11) NOT NULL,
  `descriptionniveau` text,
  PRIMARY KEY (`niveau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `OBJET` (
  `idobjet` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) DEFAULT NULL,
  `nomobjet` varchar(500) DEFAULT NULL,
  `imageobjet` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idobjet`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `PERSONNE` (
  `idpers` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `PROFESSEUR` (
  `idprof` int(11) NOT NULL AUTO_INCREMENT,
  `idpers` int(11) DEFAULT NULL,
  PRIMARY KEY (`idprof`),
  KEY `idpers` (`idpers`)
) ENGINE=InnoDB AUTO_INCREMENT=11453 DEFAULT CHARSET=utf8;

CREATE TABLE `SALLE` (
  `idsalle` int(11) NOT NULL AUTO_INCREMENT,
  `nomsalle` varchar(500) DEFAULT NULL,
  `descriptsalle` text,
  `imagesalle` varchar(1000),
  PRIMARY KEY (`idsalle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `COMPOSER_DE` (
  `idsalle` int(11) NOT NULL AUTO_INCREMENT,
  `iddev` int(11) DEFAULT NULL,
  `ordresalle` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idsalle`,`iddev` ),
  KEY `iddev` (`iddev`),
  KEY `idsalle` (`idsalle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Insertion des tables
--

INSERT INTO `APPARTIENT` (`idgr`, `netu`) VALUES
(1, 100),
(1, 45646),
(1, 54987),
(1, 265689),
(1, 458864),
(1, 564564),
(1, 2171927);

INSERT INTO `CADENA` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(1, 'Première pièce ', 5, 0, 0, 10),
(8, NULL, NULL, 0, 0, 10),
(9, NULL, NULL, 0, 0, 10),
(10, NULL, NULL, 0, 0, 10),
(11, NULL, NULL, 0, 0, 10),
(36, NULL, NULL, 0, 0, 10),
(37, NULL, NULL, 0, 0, 10),
(38, NULL, NULL, 0, 0, 10);

INSERT INTO `COMPOSER_DE` (`idsalle`, `iddev`, `ordresalle`) VALUES
(1, 1, '1'),
(2, 1, '2');

INSERT INTO `CONTIENT` (`iddm`, `idcadena`, `idsalle`) VALUES
(1, 1, 1),
(1, 8, 1),
(1, 9, 1),
(1, 10, 1),
(1, 11, 2),
(1, 36, 1),
(1, 37, 1),
(1, 38, 2);

INSERT INTO `DEVOIR` (`iddev`, `statut`, `nomDev`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`, `maxPoint`) VALUES
(1, 'En cours', 'dev1', '2018-12-08', 4, 10, 2221, 1, 500),
(3, 'fermer', NULL, '2018-12-08', 4, 10, 2221, 1, 500);

INSERT INTO `DM` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(1, 20, 1, 1),
(3, 20, 2, 3),
(4, 20, 3, 3),
(5, 20, 4, 3);

INSERT INTO `ELEVE` (`netu`, `classe`, `datec`, `idpers`) VALUES

(1, '2A22', '2018-2019', 10),
(2, '2A22', '2018-2019', 11),
(3, '2A22', '2018-2019', 12)
;

INSERT INTO `ENIGME` (`idenigme`, `nomEnigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`) VALUES
(1, 'test', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(2, 'E2', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(3, 'E3', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(4, 'E4', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1);

INSERT INTO `ENSEIGNE` (`idprof`, `idmat`) VALUES
(2221, 1),
(11452, 4);

INSERT INTO `EST_DANS` (`idenigme`, `iddev`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

INSERT INTO `EST_LIER_A` (`iddm`, `idobjet`, `idenigme`, `datetrouve`, `idsalle`) VALUES
(1, 5, 1, NULL, 1),
(1, 5, 2, NULL, 1),
(1, 5, 4, NULL, 2),
(1, 7, 3, NULL, 2);

INSERT INTO `GROUPE` (`idgr`, `nomGroupe`, `idpush`) VALUES
(1, 'groupe 1', 1),
(2, 'groupe 2', 1),
(3, 'groupe 2', 1),
(4, 'groupe 2', 1),
(5, 'Groupe 2A21 5', 100),
(6, 'Groupe 2A21 6', 54987),
(7, 'Groupe 2A21 7', 458864),
(8, 'Groupe 2A21 8', 100),
(9, 'Groupe 2A21 9', 54987),
(10, 'Groupe 2A21 10', 458864),
(11, 'Groupe 2A21 11', 100),
(12, 'Groupe 2A21 12', 54987),
(13, 'Groupe 2A21 13', 458864),
(14, 'Groupe 2A21 14', 100),
(15, 'Groupe 2A21 15', 54987),
(16, 'Groupe 2A21 16', 458864),
(17, 'Groupe 2A21 17', 100),
(18, 'Groupe 2A21 18', 54987),
(19, 'Groupe 2A21 19', 458864),
(20, 'Groupe 2A21 20', 100),
(21, 'Groupe 2A21 21', 54987),
(22, 'Groupe 2A21 22', 458864);

INSERT INTO `IS_DANS` (`idsalle`, `idobjet`, `coordobjet`, `tailleobjet`) VALUES
(1, 2, '43,222,176,322', 1),
(2, 2, '157,93,237,153', 1),
(1, 3, '120,77,205,141', 1),
(1, 10, '133,379,191,423', 1),
(2, 10, '149,250,192,282', 1);

INSERT INTO `IS_DANS_OBJET` (`idobjetSuperieur`, `idobjetInferieur`, `coordobjet2`) VALUES
(3, 4, '54,179,202,290'),
(3, 9, '205,66,302,139'),
(4, 5, '140,164,336,311'),
(4, 6, '39,159,138,233'),
(9, 7, '80,132,337,325'),
(10, 11, '117,79,438,320'),
(11, 12, '173,349,287,435');

INSERT INTO `MATIERE` (`idmat`, `nommat`) VALUES
(1, 'Math'),
(2, 'SVT'),
(3, 'Physique'),
(4, 'Informatique');

INSERT INTO `NIVEAU` (`niveau`, `descriptionniveau`) VALUES
(1, 'Le premier niveau'),
(2, 'Le second niveau');

INSERT INTO `OBJET` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(1, 'contenu', 'Table ronde', NULL),
(2, 'contenant', 'Matelas en mauvais état', NULL),
(3, 'contenant', 'Bureau', 'pics2019011781839.jpg'),
(4, 'contenu', 'Tiroir', 'pics2019011781906.jpg'),
(5, 'objet', 'ordinateur', 'pics2019011781924.jpg'),
(6, 'objet', 'cookie', 'pics2019011782032.jpg'),
(7, 'objet', 'chapeau', 'pics2019011782048.jpg'),
(8, 'objet', 'livre', 'pics2019011782100.jpg'),
(9, 'contenu', 'boite', 'pics2019011782117.jpg'),
(10, 'contenant', 'Armoir', 'pics2019011782245.jpg'),
(11, 'contenu', 'etagere', 'pics2019011782338.jpg'),
(12, 'objet', 'Debian', 'pics2019011725706.jpg');

INSERT INTO `PERSONNE` (`idpers`, `nom`, `prenom`, `mdp`, `email`) VALUES
(10, 'Paul', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(11, 'Pierre', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr'),
(12, 'Marie', 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test@gmail.com');

INSERT INTO `PROFESSEUR` (`idprof`, `idpers`) VALUES
(11452, 2),
(2221, 8),
(1, 9);

INSERT INTO `SALLE` (`idsalle`, `nomsalle`, `descriptsalle`, `imagesalle`) VALUES
(1, 'La chambre', 'Une chambre', NULL),
(2, 'Salle de jeu', 'Salle de jeu video', 'pics2019011785610.jpg');
-- --------------------------------------------------------

--
-- Contrainte des tables
--
ALTER TABLE `APPARTIENT`
  ADD CONSTRAINT `APPARTIENT_ibfk_1` FOREIGN KEY (`netu`) REFERENCES `ELEVE` (`netu`),
  ADD CONSTRAINT `APPARTIENT_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `CONTIENT`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idcadena`) REFERENCES `CADENA` (`idcadena`),
  ADD CONSTRAINT `CONTIENT_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

ALTER TABLE `DEVOIR`
  ADD CONSTRAINT `DEVOIR_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `DEVOIR_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `DM`
  ADD CONSTRAINT `DM_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `DM_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `ELEVE`
  ADD CONSTRAINT `ELEVE_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

ALTER TABLE `ENIGME`
  ADD CONSTRAINT `ENIGME_ibfk_1` FOREIGN KEY (`niveau`) REFERENCES `NIVEAU` (`niveau`),
  ADD CONSTRAINT `ENIGME_ibfk_2` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`);

ALTER TABLE `ENSEIGNE`
  ADD CONSTRAINT `ENSEIGNE_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `ENSEIGNE_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `EST_DANS`
  ADD CONSTRAINT `EST_DANS_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `EST_DANS_ibfk_2` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`);

ALTER TABLE `EST_LIER_A`
  ADD CONSTRAINT `EST_LIER_A_ibfk_1` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_2` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_4` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `IS_DANS`
  ADD CONSTRAINT `IS_DANS_ibfk_1` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `IS_DANS_OBJET`
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_1` FOREIGN KEY (`idobjetSuperieur`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_2` FOREIGN KEY (`idobjetInferieur`) REFERENCES `OBJET` (`idobjet`);

ALTER TABLE `PROFESSEUR`
  ADD CONSTRAINT `PROFESSEUR_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

ALTER TABLE `COMPOSER_DE`
  ADD CONSTRAINT `COMPOSER_DE_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `COMPOSER_DE_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
